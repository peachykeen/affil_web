'use strict';

angular.
    module('shopApp').
    controller('ListController', function($scope, ItemService) {
        $scope.results = ItemService;
        $scope.loadMore = function() {
            $scope.results.loadMore();
        };
    });