'use strict';

angular.
    module('shopApp').
    controller('DetailController', function($scope, $stateParams, ItemService, CommentService) {
        $scope.results = ItemService;
        $scope.results.selectedItem = $scope.results.getItem($stateParams.pk);
        $scope.MainImage = $scope.results.selectedItem.image;
        $scope.reviews = CommentService;
        $scope.formModel = {item: $scope.results.selectedItem.pk,};
        $scope.commentSubmit = function () {
            $scope.reviews.commentSubmit($scope.formModel);
            $scope.formModel = {item: $scope.reviews.selectedItem,};
            $scope.theForm.$setPristine();
        };
    });