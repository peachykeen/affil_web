'use strict';

angular.
    module('shopApp').
    controller('navController', function($scope, $http, ItemService, SubmitService) {
        $scope.results = ItemService;
        $scope.submits = SubmitService;
        $scope.formModel = {};
        $scope.createSubmit = function () {
            $scope.submits.createSubmit($scope.formModel)
        };
    });