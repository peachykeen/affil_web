'use strict';

angular.module('shopApp').directive('spinner', function () {
    return {
        'templateUrl': 'templates/spinner.html'
    }
});