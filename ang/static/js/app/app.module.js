'use strict';

angular.module('shopApp', [
    'ngResource',
    'infinite-scroll',
    'jcs-autoValidate',
    'angularSpinner',
    'ui.router',
    'ngAnimate',
    'toaster',
]);



