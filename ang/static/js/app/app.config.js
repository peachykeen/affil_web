'use strict';

angular.
    module('shopApp').
    config(function ($stateProvider, $urlRouterProvider, $httpProvider, $resourceProvider) {
        $stateProvider
            .state('main', {
                url: "/",
                templateUrl: 'templates/main.html',
                controller: 'ListController'
            })
            .state('detail', {
                url: "/detail/:pk",
                templateUrl: 'templates/detail.html',
                controller: 'DetailController'
            });

        $urlRouterProvider.otherwise('/');
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';    /*  token to be able to make POST request to API  */
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        $resourceProvider.defaults.stripTrailingSlashes = false;
    });

