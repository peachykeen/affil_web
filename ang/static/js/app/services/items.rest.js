'use strict';

angular.
    module('shopApp').
    factory('Items', function($resource) {
        return $resource("/api/items/:pk", {'pk': '@pk'}, {'query': {method: 'GET', isArray: true }});
});
