'use strict';

angular.
    module('shopApp').
    service('CommentService', function($rootScope, Comments, ItemService, toaster) {
        var self = {
            'loadComments': function(item) {
                console.log('loading comments!!!');
                self.selectedItem = item;
                var params = {
                    'ordering': self.ordering,
                    'item': item,
                };

                Comments.get(params, function(data) {
                    self.comments = [];
                    angular.forEach(data.results, function (comment) {
                        self.comments.push(new Comments(comment));
                    });
                });

            },
            'comments': [],
            'ordering': '-pk',

            'commentSubmit': function (comment) {
                Comments.save(comment).$promise.then(function () {
                    toaster.pop('success', 'Thank you for your review.');
                    self.loadComments(self.selectedItem);
                });
            },

        };

        return self
    });
