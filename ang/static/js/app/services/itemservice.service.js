'use strict';

angular.
    module('shopApp').service('ItemService', function(Items, $rootScope) {

        var self = {
            'getItem': function (pk) {                  //method which takes primary key as an argument and loops through the items array searching for the item with that pk, then returns the item
                for (var i = 0; i < self.items.length; i++) {
                    var obj = self.items[i];
                    if (obj.pk == pk) {
                        return obj;
                    }
                }
            },
            'page': 1,
            'hasMore': true,        // variables that store booleans which help with conditions in self object in the service
            'isLoading': false,
            'selectedItem': null,
            'items': [],          //this is the array that gets the objects from the 'results' array from the rest API by calling the loadItems function
            'search': null,
            'ordering': '-pk',
            'recommendation': null,
            'category': null,

            'doSearch': function() {   /*function for doing the search on the server, it resets all the variables
                                                and then calls loadItems() with the search string as a query parameter*/
                self.page = 1;
                self.hasMore = true;
                self.items = [];
                self.loadItems();
            },

            'doOrder': function() {
                self.page = 1;
                self.hasMore = true;
                self.items = [];
                self.loadItems();
            },

            'doFilter': function() {
                self.page = 1;
                self.hasMore = true;
                self.category = null;
                self.search = null;
                self.ordering = '-pk';
                self.items = [];
                self.loadItems();
            },

            'doCategoryFilter': function() {
                self.page = 1;
                self.hasMore = true;
                self.items = [];
                self.loadItems();
            },

            'doClear': function() {
                self.page = 1;
                self.hasMore = true;
                self.items = [];
                self.category = null;
                self.search = null;
                self.recommendation = null;
                self.ordering = '-pk';
                self.loadItems();
            },

            'loadItems': function() {           /*function, which gets the items from the API(through the Items factory).
                                Used in the initial loading of the page, in endless pagination, search query on the server etc
                                                    */
                if (self.hasMore && !self.isLoading) {
                    self.isLoading = true;

                    var params = {
                        'page': self.page,
                        'search': self.search,
                        'ordering': self.ordering,
                        'recommendation': self.recommendation,
                        'category': self.category,
                    };

                    Items.get(params, function(data) {

                        angular.forEach(data.results, function (item) {
                            self.items.push(new Items(item));
                        });
                        if (!data.next) {
                            self.hasMore = false;
                        }
                        self.isLoading = false;

                    });
                }
            },

            'loadMore': function() {
                if (self.hasMore && !self.isLoading) {
                    self.page += 1;
                    self.loadItems();
                }
            },

            /*watches for changes to the variables and then calls the filter(search etc) functions (for doing
            the filtering on the API endpoint, not on client)*/
            'watchFilters': function () {

                $rootScope.$watch(function () {
                    return self.search;
                }, function(newVal) {
                    if (angular.isDefined(newVal)) {
                        self.doSearch();
                    }
                });

                $rootScope.$watch(function () {
                    return self.ordering;
                }, function(newVal) {
                    if (angular.isDefined(newVal)) {
                        self.doOrder();
                    }
                });

                $rootScope.$watch(function () {
                    return self.recommendation;
                }, function(newVal) {
                    if (angular.isDefined(newVal)) {
                        self.doFilter();
                    }
                });

                $rootScope.$watch(function () {
                    return self.category;
                }, function(newVal) {
                    if (angular.isDefined(newVal)) {
                       self.doCategoryFilter();
                    }
                });
            },

        };

        self.loadItems();
        self.watchFilters();

        return self;
    });