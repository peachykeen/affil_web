'use strict';

angular.
    module('shopApp').
    service('SubmitService', function(Submits, $rootScope, toaster) {
        var self = {
            'isSubmitted': false,
            'createSubmit': function (email) {
                Submits.save(email).$promise.then(function () {
                    self.isSubmitted = true;
                    toaster.pop('success', 'Thank you for your email.');
                });
            },
        };

        return self
    });
