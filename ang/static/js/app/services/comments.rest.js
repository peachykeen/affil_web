'use strict';

angular.
    module('shopApp').
    factory('Comments', function($resource) {
        return $resource("/api/comments/:pk", {'pk': '@pk'}, {'query': {method: 'POST', isArray: true }});
});