'use strict';

angular.
    module('shopApp').
    factory('Submits', function($resource) {
        return $resource("/api/submits/:pk", {'pk': '@pk'});
    });
