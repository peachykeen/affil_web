from django.contrib import admin
from .models import Item, User_submit, Comment
# Register your models here.


admin.site.register(Item)
admin.site.register(User_submit)
admin.site.register(Comment)
