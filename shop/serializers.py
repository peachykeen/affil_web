from rest_framework import serializers
from .models import Item, User_submit, Comment
from django.contrib.auth.models import User

class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ('pk', 'title', 'short_description', 'image','image2', 'image3', 'image4', 'pub_date', 'price', 'recommendation', 'category', 'source_url')

class User_submitSerializer(serializers.ModelSerializer):
    class Meta:
        model = User_submit
        fields = ('pk', 'name', 'age', 'sex', 'email')

class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('pk', 'name', 'text', 'item', 'pub_date')






