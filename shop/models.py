from django.db import models

class Item(models.Model):
    title = models.CharField(max_length=250, unique=True)
    short_description=models.TextField(max_length=500, null=True, blank=True)
    pub_date = models.DateTimeField('date published', null=True, blank=True)
    image = models.ImageField(null=True, blank=True)
    image2 = models.ImageField(null=True, blank=True)
    image3 = models.ImageField(null=True, blank=True)
    image4 = models.ImageField(null=True, blank=True)
    price = models.IntegerField(null=True, blank=True)
    recommendation = models.CharField(max_length=50, null=True, blank=True)
    category = models.CharField(max_length=100, null=True, blank=True)
    source_url = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.title

class User_submit(models.Model):
    name = models.CharField(max_length=150, null=True, blank=True)
    email = models.CharField(max_length=150, null=True, blank=True)
    age = models.IntegerField(null=True, blank=True)
    sex = models.CharField(max_length=150, null=True, blank=True)

    def __str__(self):
        return self.email

class Comment(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    email = models.CharField(max_length=100, null=True, blank=True)
    pub_date = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    text = models.TextField(max_length=2000, null=True, blank=True)
    item = models.CharField(max_length=150, null=True, blank=True)

    def __str__(self):
        return self.name

