# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-17 21:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0004_auto_20161110_2208'),
    ]

    operations = [
        migrations.CreateModel(
            name='User_submit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=150, null=True)),
                ('email', models.CharField(blank=True, max_length=150, null=True)),
                ('age', models.IntegerField(blank=True, null=True)),
                ('sex', models.CharField(blank=True, max_length=150, null=True)),
            ],
        ),
    ]
