from django.shortcuts import render, render_to_response
from django.template import loader, RequestContext
from .models import Item, User_submit, Comment
from rest_framework import viewsets, filters
from .serializers import ItemSerializer, User_submitSerializer, CommentSerializer
from django_filters.rest_framework import DjangoFilterBackend



class ItemViewSet(viewsets.ModelViewSet):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter,)
    filter_fields = ('recommendation', 'category')
    search_fields = ('title', 'short_description')
    ordering_fields = ('price', 'pk')

    

class User_submitViewSet(viewsets.ModelViewSet):
    queryset = User_submit.objects.all()
    serializer_class = User_submitSerializer


class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class  = CommentSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter,)
    filter_fields = ('item',)
    ordering_fields = ('pub_date','pk',)